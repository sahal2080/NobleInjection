/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection.android;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * A singleton class for indirectly calling a registered object for performing injection.
 * This class is completely agnostic of any DI framework.
 */
public abstract class InjectionService
{
    /**
     * Interface to the code to handle injection into the given target in the given Android
     * Context.
     */
    public interface Injector
    {
        /** Perform injections on the given target based on the given Android context
         *
         * @param context The Android context that can be used for determining how to do the injection.
         * @param target The object to be injected.
         * @param <T> The type of the object to be injected.
         */
        <T> void inject(@NonNull Context context, @NonNull T target);
    }

    /**
     * The currently registered injector for the system.
     */
    @Nullable private static Injector injector;

    /**
     * Get the currently registered injector implementation.
     *
     * @return The currently registered injector
     */
    @Nullable public static Injector getInjector()
    {
        return injector;
    }

    /**
     * Register the given injector as the implementation to use for future inject calls.
     *
     * @param injector The new injector implementation to register
     */
    public static void setInjector(@Nullable Injector injector)
    {
        InjectionService.injector = injector;
    }

    /**
     * Perform injections into the given android context (e.g. Application, Activity,
     * or Service)
     *
     * @param context The context on which injection is to be performed.
     */
    public static void inject(@NonNull Context context)
    {
        inject(context, context);
    }

    /**
     * Perform injections into the given non-support library fragment
     *
     * @param fragment The fragment on which injection is to be performed.
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void inject(@NonNull Fragment fragment)
    {
        inject(fragment.getActivity(), fragment);
    }

    /**
     * Perform injections into the given support library fragment
     *
     * @param fragment The fragment on which injection is to be performed.
     */
    public static void inject(@NonNull android.support.v4.app.Fragment fragment)
    {
        inject(fragment.getActivity(), fragment);
    }

    /**
     * Perform injections into the given view
     *
     * @param view The view on which injection is to be performed.
     */
    public static void inject(@NonNull View view)
    {
        inject(view.getContext(), view);
    }

    /**
     * Perform injections into the given preference object
     *
     * @param preference The preference on which injection is to be performed.
     */
    public static void inject(@NonNull Preference preference)
    {
        inject(preference.getContext(), preference);
    }

    /**
     * Perform injections into the given target using the given context
     *
     * @param context The context to use for determining what is to be injected.
     * @param target The object on which injection is to be performed.
     * @throws IllegalStateException If no injector is registered.
     */
    public static void inject(Context context, Object target)
    {
        Injector injector = InjectionService.injector;

        if(injector == null)
        {
            throw new IllegalStateException("No injector has been registered");
        }
        else
        {
            injector.inject(context, target);
        }
    }
}
