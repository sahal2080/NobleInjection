package simpleComponentWithNestingTest;

import com.nobleworks_software.injection.ClassInjectorMap;
import com.nobleworks_software.injection.GenericComponent;
import dagger.MembersInjector;
import java.lang.IllegalArgumentException;
import javax.annotation.Generated;

@Generated("com.nobleworks_software.injection.compiler.GenericComponentProcessor")
public final class GenericOuterType_SimpleComponent extends GenericComponent<OuterType.SimpleComponent>
{
    public GenericOuterType_SimpleComponent(OuterType.SimpleComponent component)
    {
        super(component);
    }

    protected ClassInjectorMap buildInjectorMap()
    {
        ClassInjectorMap map = new ClassInjectorMap(2);

        map.put(OuterType.B.class, new SimpleInjector(0));
        return map;
    }

    private class SimpleInjector<T> implements MembersInjector<T>
    {
        private final int index;

        private SimpleInjector(int index)
        {
            this.index = index;
        }

        public void injectMembers(T instance)
        {
            switch(index)
            {
                case 0: component.inject((OuterType.B)instance);
                    break;
                default: throw new IllegalArgumentException("Invalid Index!");
            }
        }
    }
}
