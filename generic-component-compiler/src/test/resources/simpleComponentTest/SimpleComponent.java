package simpleComponentTest;

import ${componentName};

@${componentName}
interface SimpleComponent
{
    SomeInjectableType someInjectableType();
    SimpleSubcomponent simpleSubcomponent();
}
