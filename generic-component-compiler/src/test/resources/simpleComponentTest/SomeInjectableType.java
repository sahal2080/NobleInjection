package simpleComponentTest;

import javax.inject.Inject;

final class SomeInjectableType
{
    @Inject SomeInjectableType() {}
}
