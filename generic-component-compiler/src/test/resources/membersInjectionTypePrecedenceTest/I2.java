package membersInjectionTypePrecedenceTest;

import javax.inject.Inject;

interface I2 extends I
{
    @Inject void setC(C c);
}
