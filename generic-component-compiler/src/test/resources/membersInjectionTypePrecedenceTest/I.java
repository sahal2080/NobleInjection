package membersInjectionTypePrecedenceTest;

import javax.inject.Inject;

interface I
{
    @Inject void setE(E e);
}
