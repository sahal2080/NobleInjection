package membersInjectionTypePrecedenceTest;

import javax.inject.Inject;

final class A implements I
{
    @Inject public void setE(E e) {};
}
