package membersInjectorTest;

import com.nobleworks_software.injection.ClassInjectorMap;
import com.nobleworks_software.injection.GenericComponent;
import javax.annotation.Generated;

@Generated("com.nobleworks_software.injection.compiler.GenericComponentProcessor")
public final class GenericSimpleComponent extends GenericComponent<SimpleComponent>
{
    public GenericSimpleComponent(SimpleComponent component)
    {
        super(component);
    }

    protected ClassInjectorMap buildInjectorMap()
    {
        ClassInjectorMap map = new ClassInjectorMap(2);
        map.put(B.class, component.b());
        return map;
    }
}
