package membersInjectorTest;

import dagger.MembersInjector;
import ${componentName};

@${componentName}
interface SimpleComponent
{
    MembersInjector<B> b();
    void inject(B b);
}
