/*
 * Copyright (C) 2015, Dale King
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nobleworks_software.injection.compiler

import com.google.auto.common.BasicAnnotationProcessor
import com.google.common.collect.SetMultimap
import com.nobleworks_software.injection.ClassInjectorMap
import com.nobleworks_software.injection.GenericComponent
import com.nobleworks_software.injection.compiler.ComponentMethodKind.MEMBERS_INJECTOR
import com.nobleworks_software.injection.compiler.ComponentMethodKind.SIMPLE_MEMBERS_INJECTION
import com.squareup.javapoet.*
import com.sun.istack.internal.NotNull
import dagger.Component
import dagger.MembersInjector
import dagger.Subcomponent
import java.io.IOException
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*
import javax.annotation.Generated
import javax.annotation.processing.ProcessingEnvironment
import javax.lang.model.element.Element
import javax.lang.model.element.Modifier.*
import javax.lang.model.element.PackageElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.TypeMirror
import javax.lang.model.util.ElementFilter
import javax.lang.model.util.Elements
import javax.tools.Diagnostic

public class GenericComponentProcessingStep(private val processingEnv: ProcessingEnvironment)
    : BasicAnnotationProcessor.ProcessingStep
{
    val elements: Elements = processingEnv.elementUtils
    val objectElement = elements.getTypeElement(Any::class.java.canonicalName)

    override @NotNull fun annotations() = setOf(Component::class.java, Subcomponent::class.java)

    fun TypeMirror.isVisibleFrom(pkg: PackageElement): Boolean =
            processingEnv.typeUtils.asElement(this).isVisibleFrom(pkg)

    override fun process(elementsByAnnotation: SetMultimap<Class<out Annotation>, Element>)
    {
        annotations()
                .flatMap { annotation -> elementsByAnnotation.get(annotation) }
                .map { it.toType() }
                .forEach { generateObjectGraph(it) }
    }

    private fun generateObjectGraph(element: TypeElement)
    {
        val packageElement = elements.getPackageOf(element)

        val componentMethods = ElementFilter.methodsIn(elements.getAllMembers(element))
                .asSequence()
                .filter { it.enclosingElement != objectElement }
                .filter { it.isVisibleFrom(packageElement) }
                .filterNot{ it.hasQualifier() }
                .map { it.toComponentMethod(processingEnv.typeUtils, element) }
                .filterNotNull()
                .filter { it.type.isVisibleFrom(packageElement) }
                .groupBy { it.kind }

        val filteredMethods = componentMethods
                .mapValues {
                    it.value.groupBy { it.type }.mapValues {
                        val first = it.value.first()
                        if(it.value.size > 1)
                        {
                            throw IllegalStateException("Multiple methods of type ${first.kind}" +
                                    " for class ${first.type}")
                        }
                        else
                        {
                            first
                        }
                    }
                }

        val memberInjectorMethods = filteredMethods[MEMBERS_INJECTOR] ?: mapOf()
        val simpleInjectorMethods = HashMap(filteredMethods[SIMPLE_MEMBERS_INJECTION] ?: mapOf())
                .apply { keys -= memberInjectorMethods.keys }

        val injectorMethods = memberInjectorMethods + simpleInjectorMethods

        val elementName = ClassName.get(element)

        val generatedClassName = elementName.simpleNames().joinToString("_", "Generic")

        val illegalArgumentException = IllegalArgumentException::class.java

        val t = TypeVariableName.get("T")

        val classBuilder = TypeSpec.classBuilder(generatedClassName)
                .addOriginatingElement(element)
                .addAnnotation(AnnotationSpec.builder(Generated::class.java)
                        .addMember("value", "\$S",
                                GenericComponentProcessor::class.java.canonicalName).build())
                .addModifiers(PUBLIC, FINAL)
                .superclass(ParameterizedTypeName.get(ClassName.get(GenericComponent::class.java),
                        elementName))
                .addMethod(MethodSpec.constructorBuilder()
                        .addModifiers(PUBLIC)
                        .addParameter(elementName, "component")
                        .addStatement("super(component)").build())

        val injectorMapType = ClassName.get(ClassInjectorMap::class.java)

        val hashMapSize = determineHashMapSize(injectorMethods.size)

        val injectionMapBuilder = MethodSpec.methodBuilder("buildInjectorMap")
            .addModifiers(PROTECTED)
            .returns(injectorMapType)
            .addStatement("\$T map = new \$T(\$L)", injectorMapType, injectorMapType, hashMapSize)

        memberInjectorMethods.values.sortedBy({it.type.toString()}).forEach {
            injectionMapBuilder.addStatement("map.put(\$T.class, component.\$L())",
                    it.type, it.name)
        }

        if(!simpleInjectorMethods.isEmpty())
        {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            pw.println("The following types have only simple injection methods defined")
            pw.println("which causes a less efficient generic component implementation.")
            pw.println("It would be a good idea to define MembersInjector methods for them.")
            val injectMethodSpec = MethodSpec.methodBuilder("injectMembers")
                    .addModifiers(PUBLIC)
                    .addParameter(t, "instance")
                    .beginControlFlow("switch(index)")

            simpleInjectorMethods.values.sortedBy({it.type.toString()}).forEachIndexed { i, descriptor ->
                injectMethodSpec.addStatement("case \$L: component.\$L((\$T)instance)", i,
                        descriptor.name, descriptor.type)
                injectMethodSpec.addStatement("\$>break\$<")
                injectionMapBuilder.addStatement("map.put(\$T.class, new SimpleInjector(\$L))",
                        descriptor.type, i)
                pw.print("    ")
                pw.print(descriptor.type)
            }

            pw.close()
            processingEnv.messager.printMessage(Diagnostic.Kind.WARNING, sw.toString())

            injectMethodSpec
                    .addStatement("default: throw new \$T(\"Invalid Index!\")", illegalArgumentException)
                    .endControlFlow()

            val adapterBuilder = TypeSpec.classBuilder("SimpleInjector")
                    .addModifiers(PRIVATE)
                    .addTypeVariable(t)
                    .addSuperinterface(ParameterizedTypeName.get(
                            ClassName.get(MembersInjector::class.java), t))
                    .addField(TypeName.INT, "index", PRIVATE, FINAL)
                    .addMethod(MethodSpec.constructorBuilder()
                            .addModifiers(PRIVATE)
                            .addParameter(TypeName.INT, "index")
                            .addStatement("this.index = index")
                            .build())
                    .addMethod(injectMethodSpec.build())

            classBuilder.addType(adapterBuilder.build())
        }

        injectionMapBuilder.addStatement("return map")

        classBuilder.addMethod(injectionMapBuilder.build())

        try
        {
            JavaFile.builder(elementName.packageName(), classBuilder.build())
                    .build().writeTo(processingEnv.filer)
        }
        catch (ioe: IOException)
        {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            pw.println("Error generating source file for type " + classBuilder.build().name)
            ioe.printStackTrace(pw)
            pw.close()
            processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, sw.toString())
        }
    }
}
