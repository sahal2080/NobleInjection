package frogermcs.io.githubclient.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nobleworks_software.injection.android.InjectionService;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import frogermcs.io.githubclient.R;
import frogermcs.io.githubclient.activities.Henson;
import frogermcs.io.githubclient.data.model.User;
import frogermcs.io.githubclient.presenters.SplashViewPresenter;
import frogermcs.io.githubclient.utils.AnalyticsManager;
import frogermcs.io.githubclient.utils.SimpleObserver;
import frogermcs.io.githubclient.views.SplashView;
import rx.android.widget.OnTextChangeEvent;
import rx.android.widget.WidgetObservable;

public class SplashActivity extends AppCompatActivity implements SplashView
{
    @Bind(R.id.etUsername)
    EditText etUsername;

    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;

    @Bind(R.id.btnShowRepositories)
    Button btnShowRepositories;

    @Inject
    SplashViewPresenter presenter;

    @Inject
    AnalyticsManager analyticsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        InjectionService.inject(this);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        presenter.bindView(this);

        analyticsManager.logScreenView(getClass().getName());

        WidgetObservable.text(etUsername, true).subscribe(new SimpleObserver<OnTextChangeEvent>()
        {
            @Override
            public void onNext(OnTextChangeEvent onTextChangeEvent)
            {
                presenter.setUserName(onTextChangeEvent.text().toString());
                etUsername.setError(null);
            }
        });

        etUsername.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                boolean handled = actionId == EditorInfo.IME_ACTION_GO;

                if (handled)
                {
                    onShowRepositoriesClick();
                }
                return handled;
            }
        });
    }

    @OnClick(R.id.btnShowRepositories)
    public void onShowRepositoriesClick()
    {
        presenter.showRepositories();
    }

    public void showRepositoriesListForUser(User user)
    {
        startActivity(Henson.with(this).gotoRepositoriesListActivity().user(user).build());
    }

    public void showValidationError()
    {
        etUsername.setError("Validation error");
    }

    public void showLoading(boolean loading)
    {
        btnShowRepositories.setVisibility(loading ? View.GONE : View.VISIBLE);
        pbLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
