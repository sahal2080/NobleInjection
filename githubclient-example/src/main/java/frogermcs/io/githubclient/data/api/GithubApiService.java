package frogermcs.io.githubclient.data.api;

import java.util.List;

import frogermcs.io.githubclient.data.api.response.RepositoryResponse;
import frogermcs.io.githubclient.data.api.response.UserResponse;
import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by Miroslaw Stanek on 22.04.15.
 */
public interface GithubApiService {

    @GET("/users/{userName}")
    Observable<UserResponse> getUser(
            @Path("userName") CharSequence username
    );

    @GET("/users/{userName}/repos")
    Observable<List<RepositoryResponse>> getUsersRepositories(
            @Path("userName") CharSequence username
    );
}
