# NobleInjection
## Objective
Enable a type, when using [Dagger 2][1] on Android (or similar frameworks where the objects that need dependency injection are created by the framework), to:
- Request injection with no extra boilerplate
- Be completely decoupled from how actual injection is done

To achieve this we need to add to [Dagger 2][1] antoher 2 pieces.

## Piece 1: AnnotationProcessor
Generates wrapper classes around [Dagger 2][1] `component` and `subcomponent` to allow injection into an object using a single generic method.
- Allows you to inject any type that the component knows how to inject
- No more code statically bound at compile-time to the injection method
- Keep objects needing to be injected from being coupled to the component providing the dependencies

The generic wrapper class has generic methods defined on it (specified by interfaces):

```java
    /**
     * Perform member injection on the given instance. If there is no injection method defined
     * for the object's class or any of its superclasses throws an
     * <code>IllegalArgumentException</code>
     *
     * @param instance Object to be injected
     * @param <T> The type of the object
     * @throws IllegalArgumentException if no injection is defined for the class or any of its
     *      superclasses
     */
    <T> void injectMembers(T instance);

    /**
     * Get a member injector for the given object if one is defined for the given object
     *
     * @param instance The object to retrieve a <code>MembersInjector</code> for
     * @param <T> The type of the object
     * @return A <code>MembersInjector</code> instance to inject the given object or null if
     *     no injection is defined for the given class
     */
    <T> MembersInjector<? super T> getInjector(T instance);
```

The name of the generated wrapper class will be the name of the [Dagger 2][1] component with the word `Generic` prepended to it and will be in the same package as the component (e.g. `com.foo.AppComponent` will generate a `com.foo.GenericAppComponent` class).

> While the annotation processor can work with either the inject form of component methods or with the component methods that return a `MembersInjector`, the latter will generate a more straight-forward, efficient implementation. The annotation processor will warn you if you only use the inject methods for a type.

## Piece 2: Android Injection Service
Singleton service that enables a simple call for Android-controlled objects (Application, Activity, Fragment, View, Service, Preference) to request an injection but not be coupled to how its done.
- Create an implementation of this interface in your app. You can base it on the example application's [implementation](https://github.com/NobleInjection/NobleInjection/blob/master/githubclient-example/src/main/java/frogermcs/io/githubclient/di/GitHubClientInjector.java)

> This example is complex to demonstrate use of `scope`s (based on the type of Android context and whether the object implements the `HasUser` interface).

- Register the implementation in your app initialization:

```java
    InjectionService.setInjector(new MyInjectorImplementation());
```

- Request an injection in your class by adding this at the appropriate point in their lifecycle (can also be done in a common base class):

```java
    InjectionService.inject(this);
```

Injection now will be based on the actual instance and the Android context and is done using `InjectionService.Injector` interface which has a single method:

```java
    public interface Injector
    {
        /** Perform injections on the given target based on the given Android context
         *
         * @param context The Android context that can be used for determining how to do the injection.
         * @param target The object to be injected.
         * @param <T> The type of the object to be injected.
         */
        <T> void inject(@NonNull Context context, @NonNull T target);
    }
```

## Installation
The libraries are built using [JitPack.io][3] so you will need first to add [https://jitpack.io](https://jitpack.io) as a maven repository to your project.

So to use in Gradle:

```groovy
buildscript {
  dependencies {
    classpath 'com.neenbedankt.gradle.plugins:android-apt:1.8'
  }
}

repositories {
    jcenter()
    maven{ url "https://jitpack.io" }
}
```

### AnnotationProcessor
To use there are two libraries, the annotation processor itself and a small runtime library containing some classes for the implementation.

```groovy
apply plugin: 'com.neenbedankt.android-apt'

dependencies {
    compile 'com.gitlab.NobleworksSoftware.NobleInjection:generic-component-runtime:1.0.0'
    apt 'com.gitlab.NobleworksSoftware.NobleInjection:generic-component-compiler:1.0.0'
}
```

### Android Injection Service
To use add this dependency:

```groovy
    compile 'com.gitlab.NobleworksSoftware.NobleInjection:android-injection-service:1.0.0'
```

## Bonus: Kotlin!
I have also created a Kotlin specific version of the injection service that uses extension functions for more compact syntax.

To register an implementation of the injector the syntax would be:

```java
    new MyInjectorImplementation().setAsInjector()
```

The classes that need to request injection (e.g. Activity) simply add a line of this form at the appropriate point in their lifecycle (which can actually be done in a common base class):

```kotlin
    inject()
```

If you have some other class than the ones that have extension methods defined for you can request injection using a specific Android context using this statement:

```kotlin
    context inject this
```

The dependency for the kotlin version is:

```groovy
    compile 'com.gitlab.NobleworksSoftware.NobleInjection:android-injection-service-kotlin:1.0.0'
```

## Discussion: The Problem
### Plain Java & DI
In a typical plain Java application using dependency injection all of the objects needed by the application can be created by the DI framework. The interaction with the DI framework is very simple. In the main you create the component, get a root object for the app and start it.

### Android et al & DI
This is not the case in Android and in some other frameworks (e.g. Servlets). Many of the classes in Android (Application, Activity, Fragment, Service, etc.) are created by the Android framework itself . To do dependency injection on these objects, each needs to take some action to cause itself to be injected at the appropriate time in its lifecycle. We need to do this in such a way that the object requesting injection has no knowledge or coupling to the actual component doing the injection or we lose any actual benefit of the DI framework (loose coupling, the ability to choose what gets injected at runtime).

#### Dagger 1
Loose coupling was possible because injection was done in the ObjectGraph using a generic inject method that could accept any type of object and try to inject it.

#### [Dagger 2][1]
Significantly changed design made the process of an object causing itself to be injected with no coupling to the actual injector very difficult and cumbersome requiring a large amount of boilerplate in each type needing injection.

In [Dagger 2][1] you have to declare a method on the `component` for each type that needs to be injected after construction. That method must either be a direct injection method of the form:

```java
    void inject(Foo f);
```

Or a method that returns a `MemberInjector` for the type, as in:

```java
    MembersInjector<Foo> getFooInjector();
```

Java does not have any form of [Multiple Dispatch][2] (where it can choose the right method to call based on the run-time type of an argument). This means:
- For each method there must be code to call it with correct compile-time type, and so
- Each type to be injected must include a call to the corresponding method

**How does that type get a reference to implementation of that method and remain loosely coupled so, at runtime, a different implementation of this method can be chosen (e.g. for running unit tests on the class vs. using it in production)?**

Out of the box, there is no clean, concise ways to do this with [Dagger 2][1].

> **Warning** Most of the examples seen online fail miserably on this. Some examples actually instantiate the component to do the injection inside the class needing injection! This has no loose coupling and actually ties that class to only that one component, making it impossible to substitute different implementations at run-time.

The only way to completely decouple the type needing injection from the component is to declare an injection interface for each type that needs injection and a Visitor method as well that accepts an instance of that interface and calls it to do the injection. The component interface itself is declared to extend each of the declared interfaces.

```java
public class FooActivity extends AppCompatActivity implements Injectable&lt;FooActivity.Injector&gt;
{
    public interface Injector ( void inject(FooActivity activity); }

    @Override
    public void inject(Injector injector) { injector.inject(this) }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        InjectionHelper.inject(this);
```

Then your component looks like this:

```java
@Component({MyModule.class})
public interface MyComponent extends
     FooActivity.Injector,
     BarActivity.Injector,
     ...
{
}
```

First thing to notice is that this is a LOT of boilerplate code that has to be added to every type needing injection. We must ask ourselves why are we going to all this extra hassle.

One of the design goals of [Dagger 2][1] was that injection could be verified at compile-time and would not fail at runtime. This design goal works well for the straight Java example where the only interaction with DI is to create a component in the main method and call a get method on it to get the complete application. In cases like Android where objects cannot be created by Dagger and instead they have to request that they are injected it does not really work.

It may appear that all this boilerplate at least serves a beneficial purpose of maintaining this compile time type safety, but in reality that is an illusion. If you look into the possibilities for this `InjectionHelper` that I added in there its implementaiton will almost certainly involve a cast to the injector type that can fail at run time so is not really compile-time type safe.

If you think about it, it only makes sense. The decision of what component does the injection and thus what actual values get injected should take place at runtime. There is no way that can actually be verified at compile time.

So the point of this project is to provide a way to allow a type to request injection with no extra boilerplate and be completely decoupled from how that actual injection is done.

For more on this topic please see:
- [https://gitlab.com/NobleworksSoftware/NobleInjection](https://gitlab.com/NobleworksSoftware/NobleInjection)
- [https://github.com/google/dagger/issues/213](https://github.com/google/dagger/issues/213)
- [https://github.com/tbroyer/bullet/issues/2](https://github.com/tbroyer/bullet/issues/2)

## License

```
Copyright (C) 2015, Dale King

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

[1]: http://google.github.io/dagger/
[2]: https://en.wikipedia.org/wiki/Multiple_dispatch
[3]: http://jitpack.io